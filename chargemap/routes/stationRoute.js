"use strict";

const router = require("express").Router();
// const passport = require("../utils/pass");
const stationController = require("../controllers/stationController.js");

router
  .route("/")
  .get(stationController.getAllStation)
  .post(
    // passport.authenticate("jwt", { session: false }),
    stationController.createStation
  )
  .put(
    // passport.authenticate("jwt", { session: false }),
    stationController.modifyStation
  );
router
  .route("/:id")
  .get(stationController.getStationById)
  .delete(stationController.deleteStation);

module.exports = router;
