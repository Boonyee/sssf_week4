const { gql } = require("apollo-server-express");

module.exports = gql`
  type Station {
    id: ID!
    Location: Location!
    Connections: [Connection]!
    Title: String
    AddressLine1: String
    Town: String
    StateOrProvince: String
    Postcode: String
  }

  extend type Query {
    stations(start: Int = 0, limit: Int = 10, bounds: BoundInput): [Station]
    station(id: ID!): Station
  }

  extend type Mutation {
    addStation(
      Connections: [ConnectionInput]
      Postcode: String
      Title: String
      AddressLine1: String
      StateOrProvince: String
      Town: String
      Location: LocationInput!
    ): Station

    deleteStation(id: ID!): Station

    modifyStation(
      id: ID!
      Connections: [ConnectionInput]
      Postcode: String
      Title: String
      AddressLine1: String
      StateOrProvince: String
      Town: String
    ): Station
  }

  input ConnectionInput {
    id: ID
    ConnectionTypeID: ID
    CurrentTypeID: ID
    LevelID: ID
    Quantity: Int
  }

  input LocationInput {
    coordinates: [Float!]!
  }

  input BoundInput {
    _southWest: Point!
    _northEast: Point!
  }

  input Point {
    lat: Float!
    lng: Float!
  }
`;
