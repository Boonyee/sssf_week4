const passport = require('./pass.js');

module.exports = (req, res) => {
  try {
    return new Promise((resolve, reject) => {
      passport.authenticate(
        'jwt',
        {
          session: false,
        },
        (err, user, info) => {
          if (!user) {
            resolve(false);
          }
          resolve(user);
        }
      )(req, res);
    });
  } catch (err) {
    throw err;
  }
};
