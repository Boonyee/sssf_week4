const Level = require("../models/Level.js");

module.exports = {
  Query: {
    leveltypes: () => {
      return Level.find();
    },
  },
  Connection: {
    LevelID(parent) {
      return Level.findById(parent.LevelID);
    },
  },
  //   Mutation: {
  //     addSpecies: (parent, args) => {
  //       const newSpecies = new Species(args);
  //       return newSpecies.save();
  //     },
  //   },
};
