const levelResolver = require('./levelResolver.js');
const currentTypeResolver = require('./currentTypeResolver.js');
const connectionTypeResolver = require('./connectionTypeResolver.js');
const stationResolver = require('./stationResolver.js');
const connectionResolver = require('./connectionResolver.js');
const userResolver = require('./userResolver.js');

module.exports = [
  levelResolver,
  currentTypeResolver,
  connectionTypeResolver,
  stationResolver,
  connectionResolver,
  userResolver,
];
