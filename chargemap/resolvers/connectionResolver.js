const Connection = require("../models/Connection.js");

module.exports = {
  Station: {
    Connections(parent) {
      return parent.Connections.map((id) => Connection.findById(id));
    },
  },
  //   Mutation: {
  //     addSpecies: (parent, args) => {
  //       const newSpecies = new Species(args);
  //       return newSpecies.save();
  //     },
  //   },
};
