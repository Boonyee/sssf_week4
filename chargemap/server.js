'use strict';
require('dotenv').config();
const express = require('express');
const app = express();
const stationRoute = require('./routes/stationRoute.js');
const authRoute = require('./routes/authRoute.js');
const userRoute = require('./routes/userRoute.js');

const db = require('./utils/db.js');
const passport = require('./utils/pass.js');
const helmet = require('helmet');
const checkAuth = require('./utils/checkAuth.js');

const schemas = require('./schemas/index.js');
const resolvers = require('./resolvers/index.js');
const { ApolloServer } = require('apollo-server-express');

// http and https
const http = require('http');
const https = require('https');
const fs = require('fs');
const sslkey = fs.readFileSync('ssl-key.pem');
const sslcert = fs.readFileSync('ssl-cert.pem');

const options = {
  key: sslkey,
  cert: sslcert,
};

app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(
  helmet({
    ieNoOpen: false,
    contentSecurityPolicy: false,
  })
);

app.use('/station', stationRoute);
app.use('/auth', authRoute);
app.use('/user', userRoute);

app.get('/', (req, res) => {
  res.send('please proceed to /graphql <a href="/graphql">here</a>');
});

db.on('connected', () => {
  try {
    const server = new ApolloServer({
      typeDefs: schemas,
      resolvers,
      context: async ({ req, res }) => {
        if (req) {
          const user = await checkAuth(req, res);
          // console.log("app", user);
          return {
            req,
            res,
            user,
          };
        }
      },
    });

    server.applyMiddleware({ app });

    http
      .createServer((req, res) => {
        res.writeHead(301, {
          Location: `https://localhost:${process.env.PORT_HTTPS}` + req.url,
        });
        res.end();
      })
      .listen(process.env.PORT_HTTP);

    https
      .createServer(options, app)
      .listen({ port: process.env.PORT_HTTPS }, () =>
        console.log(
          `🚀 Server ready at https://localhost:${process.env.PORT_HTTPS}${server.graphqlPath}`
        )
      );
  } catch (e) {
    throw e;
  }
});
