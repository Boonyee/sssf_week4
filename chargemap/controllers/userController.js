'use strict';
const User = require('../models/User.js');

const createUser = async (req, res) => {
  try {
    const doc = await User.create({ ...req.body });
    const result = doc.toJSON();
    // console.log("doc", doc);
    return res.json({ ...result, password: undefined });
  } catch (err) {
    if (err.name === 'MongoError' && err.code === 11000) {
      // Duplicate username
      return res
        .status(422)
        .send({ succes: false, message: 'User already exist!' });
    }

    // Some other error
    return res.status(422).send(err);
  }
};

module.exports = { createUser };
