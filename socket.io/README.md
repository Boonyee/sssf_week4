# sssf-w4-socketio

#### sssf week 4 exercise socket.io

- Both client & server connect
- Receiving messages: socket.on('message', ...
- Client can be identified by session.id
- Server has rooms, channels for grouping messages

#### The system works with commands:

`/help` for commands <br>
`/join <channelname>` to join a channel <br>
`/leave` to go back to the global channel

Nickname is client-sided and is sent as a part of a message. If no nickname is defined, the user is anonymous (socket.id hash was way too long and distracting in chat window, yet the server identifies users with the socket.id as a proof of concept).

#### Additional tasks:

> What are namespaces in Socket.io? How they are different from rooms?

Namespaces are like containers for different instances of the socket.io. A server can, for example, have a namespace "a" and "b", both containing the room "c". However, as the namespaces are individual, users in namespace "a" cannot straight up interact with users in namespace "b" even if they seemingly are in channel "c". A bridge can be done, for example when sending a message from room "c" of namespace "b" to the room "c" of namespace "a" with a `io.of('a').to('c').emit('chat message', 'hello');`.


> Let us know how you could utilize websockets in your project.

As my project is an IRC styled instant messaging app, I see the potential of websockets straight up inspiring. Before this part of the course, I had no idea how would I implement a real time communication system. Websockets introduce a proper way to handle real time communication system, while still keeping a possibility to have an underlying database and personalized behaviour.<br> I tried to implement some command message logic for this assignment, as the project is going to heavily utilize commands instead of buttons. I, however, still have to improve that at least for personal sanitys sake; the seemingly never-ending if-elseif-else solution is horrible and pretty unreadable.